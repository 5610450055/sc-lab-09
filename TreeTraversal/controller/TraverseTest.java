package controller;

import model.InOrderTraversal;
import model.Node;
import model.PostOrderTraversal;
import model.PreOrderTraversal;
import model.ReportConsole;


public class TraverseTest {

	public static void main(String[] args) {
		
		Node node1 = new Node("A",null,null);
		Node node2 = new Node("C",null,null);
		Node node3 = new Node("E",null,null);
		Node node4 = new Node("H",null,null);
		Node node5 = new Node("D",node2,node3);
		Node node6 = new Node("I",node4,null);
		Node node7 = new Node("B",node1,node5);
		Node node8 = new Node("G",null,node6);
		Node node9 = new Node("F",node7,node8);
		
		InOrderTraversal in = new InOrderTraversal();
		PreOrderTraversal pre = new PreOrderTraversal();
		PostOrderTraversal post = new PostOrderTraversal();
		ReportConsole report = new ReportConsole();
		
		System.out.print("Traverse with PreOrderTraversal: ");
		report.display(node9, pre);
		
		System.out.print("Traverse with InOrderTraversal: ");
		report.display(node9, in);
		
		System.out.print("Traverse with PostPreOrderTraversal: ");
		report.display(node9, post);

	}

}
