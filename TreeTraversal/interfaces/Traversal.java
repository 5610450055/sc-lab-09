package interfaces;

import java.util.ArrayList;

import model.Node;

public interface Traversal {
	
	public ArrayList<Node> traverse(Node node);

}
