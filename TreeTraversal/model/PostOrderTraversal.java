package model;

import java.util.ArrayList;

import interfaces.Traversal;

public class PostOrderTraversal implements Traversal {
	
	ArrayList<Node> post = new ArrayList<Node>();
	
	@Override
	public ArrayList<Node> traverse(Node node) {
		
		Node n = node;
		if (n==null) return null;
		  
		traverse(n.getLeft());
		traverse(n.getRight()); 
		post.add(n);
		
		return post;
	}

}
