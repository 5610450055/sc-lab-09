package model;

import java.util.ArrayList;

import interfaces.Traversal;

public class InOrderTraversal implements Traversal {

	ArrayList<Node> in = new ArrayList<Node>();

	@Override
	public ArrayList<Node> traverse(Node node) {
		
		Node n = node;
		if (n==null) return null;

		traverse(n.getLeft());
		in.add(n);
		traverse(n.getRight()); 
		
		return in;
	}

}
