package model;

import interfaces.Traversal;

public class ReportConsole {
	
	public void display(Node node,Traversal traversal){
		for(Node a:traversal.traverse(node)){
			System.out.print(a.getValue()+" ");
			}
		System.out.println();

	}

}
