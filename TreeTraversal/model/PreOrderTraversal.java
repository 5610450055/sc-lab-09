package model;

import java.util.ArrayList;

import interfaces.Traversal;

public class PreOrderTraversal implements Traversal {

	ArrayList<Node> pre = new ArrayList<Node>();
	
	@Override
	public ArrayList<Node> traverse(Node node) {

		Node n = node;
		if (n==null) return null;
		  
		pre.add(n); 
		traverse(n.getLeft());
		traverse(n.getRight()); 
		
		return pre;
	}

}
