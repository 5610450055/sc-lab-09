package model;

import interfaces.Taxable;

import java.util.Comparator;

public class TaxableComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable other1, Taxable other2) {
		
		if (other1.getTax() < other2.getTax()){return -1;}
		if (other1.getTax() > other2.getTax()){return 1;}
		return 0;
	}

}
