package model;

import interfaces.Taxable;


public class Person implements Taxable, Comparable<Person> {
	
	private String name;
	private double salary;
	
	public Person(String name, double salary) {
		this.name = name;
		this.salary = salary;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public double getTax() {
		
		if (salary <=300000) return salary*0.05;
		
		else return (salary*0.05)+((salary-300000)*0.1);
		
	}


	@Override
	public int compareTo(Person other) {
		if (this.salary < other.salary ) { return -1; }
		if (this.salary > other.salary ) { return 1;  }
		return 0;
	}
	
	public String toString() {
		return "Person: Name = "+this.name+", Salary/Year = "+this.salary;
	}


}
