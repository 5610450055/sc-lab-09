package model;

import interfaces.Taxable;


public class Company implements Taxable {
	
	private String companyName;
	private double earning;
	private double expense;
	private double profit;
	
	public Company(String comName, double earning, double expense) {
		this.companyName = comName;
		this.earning = earning;
		this.expense = expense;
		this.profit = earning-expense;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	@Override
	public double getTax() {
		return ((earning-expense)*0.3);
	}
	
	public double getProfit() {
		return (earning-expense);
	}
	
	public double getEarning(){
		return earning;
	}
	
	public double getExpense(){
		return expense;
	}
	
	public String toString(){
		return "Company: Name = "+this.companyName+", Earning = "+this.earning+", Expense = "+this.expense+", Profit = "+this.profit;
	}

}
