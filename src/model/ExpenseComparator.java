package model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company> {


	@Override
	public int compare(Company other1, Company other2) {
		double expense1 = other1.getExpense();
		double expense2 = other2.getExpense();
		if (expense1 > expense2) return 1;
		if (expense1 < expense2) return -1;
		return 0;
	}

}
