package model;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company> {


	@Override
	public int compare(Company other1, Company other2) {
		double profit1 = other1.getProfit();
		double profit2 = other2.getProfit();
		if (profit1 > profit2) return 1;
		if (profit1 < profit2) return -1;
		return 0;
	}

}
