package model;

import interfaces.Taxable;


public class Product implements Taxable, Comparable<Product> {
	
	private String productName;
	private double price;
	
	public Product(String proName, double price) {
		this.productName = proName;
		this.price = price;
	}

	public String getProName(){
		return productName;
	}
	
	@Override
	public double getTax() {
		return price*0.07;
	}

	@Override
	public int compareTo(Product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
	
	public String toString() {
		return "Product: Name = "+this.productName+", Price = "+this.price;
	}


}
