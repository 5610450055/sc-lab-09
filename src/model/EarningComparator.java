package model;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company> {
	

	@Override
	public int compare(Company other1, Company other2) {
		double earning1 = other1.getEarning();
		double earning2 = other2.getEarning();
		if (earning1 > earning2) return 1;
		if (earning1 < earning2) return -1;
		return 0;
	}

}
