package controller;

import interfaces.Taxable;

import java.util.ArrayList;
import java.util.Collections;

import model.Company;
import model.EarningComparator;
import model.ExpenseComparator;
import model.Person;
import model.Product;
import model.ProfitComparator;
import model.TaxableComparator;

public class Tester {
	
	public static void main(String[] args) {
			
		System.out.println("\n------------------- Person ---------------------");
			ArrayList<Person> persons = new ArrayList<Person>();
			persons.add(new Person("Robert", 350000));
			persons.add(new Person("Owen", 540000));
			persons.add(new Person("Steve", 210000));
	
			System.out.println("---Before sort salary/year in person");
			for (Person per : persons) 
				System.out.println(per);
	
			Collections.sort(persons);
			
			System.out.println("\n---After sort salary/year in person");
			for (Person per : persons) 
				System.out.println(per);
	
			System.out.println("\n------------------- Product ---------------------");
			ArrayList<Product> products = new ArrayList<Product>();
			products.add(new Product("Digital T.V. Full HD", 20000));
			products.add(new Product("Furniture Full Set", 125000));
			products.add(new Product("Microwave", 7500));
			
			System.out.println("---Before sort price in Product");
			for (Product pro : products) 
				System.out.println(pro);
	
			Collections.sort(products);
			
			System.out.println("\n---After sort price in Product");
			for (Product pro : products) 
				System.out.println(pro);
			
			
			
			System.out.println("\n------------------- Company ---------------------");
			ArrayList<Company> companies = new ArrayList<Company>();
			companies.add(new Company("Avenue Co.,Ltd.",750000,350000));
			companies.add(new Company("HeadHunter Co.,Ltd.",1000000,75000));
			companies.add(new Company("BoszBistro Ltd.",450000,150000));
			
			System.out.println("---Before sort with Earning");
			for (Company c : companies) {
				System.out.println(c);
			}
			
			Collections.sort(companies, new EarningComparator());
			
			System.out.println("\n----After sort with Earning");
			for (Company c : companies) {
				System.out.println(c);
			}
			
			Collections.sort(companies, new ExpenseComparator());
			
			System.out.println("\n---- After sort with Expense");
			for (Company c : companies) {
				System.out.println(c);
			}
			
			Collections.sort(companies, new ProfitComparator());
			
			System.out.println("\n---- After sort with Profit");
			for (Company c : companies) {
				System.out.println(c);
			}
			
			
			System.out.println("\n------------------- Taxable ---------------------");
			ArrayList<Taxable> tax = new ArrayList<Taxable>();
			tax.add(new Person("Mr.A",350000));
			tax.add(new Person("Mr.B",100000));
			tax.add(new Product("Gold-Necklace",15000));
			tax.add(new Product("Air-Conditioner",27000));
			tax.add(new Company("Avenue Co.,Ltd.",750000,350000));
			tax.add(new Company("BoszBistro Ltd.",450000,150000));
				
			System.out.println("----Before sort with Taxable");
			for (Taxable c : tax) {
				System.out.println(c);
			}
			
			Collections.sort(companies, new TaxableComparator());
			
			System.out.println("\n----After sort with Taxable");
			for (Taxable c : tax) {
				System.out.println(c);
			}
	
	
	
		}

}
